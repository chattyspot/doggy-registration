package com.chattyspot.doggy

import com.chattyspot.doggy.data.BreedRepository
import com.chattyspot.doggy.data.ServiceTypeRepository
import com.chattyspot.doggy.models.Breed
import com.chattyspot.doggy.models.ServiceType
import com.chattyspot.doggy.utils.Log
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class Application {

    @Bean
    fun persistBreeds(breedRepository: BreedRepository): CommandLineRunner {
        Log.info("persistBreeds is called")
        return CommandLineRunner {
            breedRepository.save(Breed(null, "Hound"))
            breedRepository.save(Breed(null, "Terrier"))
            breedRepository.save(Breed(null, "Malamute"))
            breedRepository.save(Breed(null, "Spaniel"))
            breedRepository.save(Breed(null, "Sennenhund"))
        }
    }

    @Bean
    fun persistDogServiceTypes(
            serviceTypeRepository: ServiceTypeRepository
    ): CommandLineRunner {
        Log.info("persistDogServiceTypes called")
        return CommandLineRunner {
            serviceTypeRepository.save(ServiceType(null, "Hearing"))
            serviceTypeRepository.save(ServiceType(null, "Seizure"))
            serviceTypeRepository.save(ServiceType(null, "Guide"))
            serviceTypeRepository.save(ServiceType(null, "PTSD"))
            serviceTypeRepository.save(ServiceType(null, "Military"))
        }
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}