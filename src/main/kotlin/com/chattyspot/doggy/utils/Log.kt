package com.chattyspot.doggy.utils

import org.slf4j.Logger
import org.slf4j.LoggerFactory

private val log: Logger = LoggerFactory.getLogger("ZavLog")

object Log {

    fun info(message: String) = log.info(message)

    fun debug(message: String) = log.debug(message)

    fun error(message: String, throwable: Throwable) = log.error(message, throwable)
}