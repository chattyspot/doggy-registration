package com.chattyspot.doggy.controller

import org.springframework.stereotype.Controller
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Controller
class WebController : WebMvcConfigurer {

    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.addViewController("/error").setViewName("error")
    }
}