package com.chattyspot.doggy.controller

import com.chattyspot.doggy.data.BreedRepository
import com.chattyspot.doggy.data.DogRepository
import com.chattyspot.doggy.data.ServiceTypeRepository
import com.chattyspot.doggy.models.Dog
import com.chattyspot.doggy.utils.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.mvc.support.RedirectAttributes

@Controller
@RequestMapping("/")
class HomepageController(

        @Autowired
        private val breedRepository: BreedRepository,

        @Autowired
        private val dogRepository: DogRepository,

        @Autowired
        private val serviceTypeRepository: ServiceTypeRepository
) {

    @GetMapping
    fun showRegisterNewDogForm(model: Model): String {
        Log.info("showRegisterNewDogForm")

        val breeds = breedRepository.findAll()
        model.addAttribute("breeds", breeds)

        val serviceTypes = serviceTypeRepository.findAll()
        model.addAttribute("serviceTypes", serviceTypes)

        model.addAttribute("dog", Dog())

        return "home"
    }

    @PostMapping
    fun registerNewDog(
            @ModelAttribute("dog")
            dog: Dog,
            model: Model,
            redirectAttributes: RedirectAttributes
    ): String {
        Log.info("registerNewDog is called")

        Log.info("The dog to save is $dog")
        dogRepository.save(dog)
        redirectAttributes.addFlashAttribute("saved-dog", dog)

        return "redirect:all-dogs"
    }
}