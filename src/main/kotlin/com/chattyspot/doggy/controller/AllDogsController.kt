package com.chattyspot.doggy.controller

import com.chattyspot.doggy.data.DogRepository
import com.chattyspot.doggy.models.Dog
import com.chattyspot.doggy.utils.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/all-dogs")
class AllDogsController(
        @Autowired
        private val dogRepository: DogRepository
) {

    @GetMapping
    fun showAllDogs(
            @ModelAttribute("saved-dog")
            dog: Dog,
            model: Model
    ): String {
        Log.info("showAllDogs is called")

        Log.info("The dog to show is $dog")
        model.addAttribute("dog", dog)

        val dogs = dogRepository.findAll()
        model.addAttribute("dogs", dogs)

        return "all-dogs"
    }
}