package com.chattyspot.doggy.data

import com.chattyspot.doggy.models.ServiceType
import org.springframework.data.repository.CrudRepository

interface ServiceTypeRepository : CrudRepository<ServiceType, Long>