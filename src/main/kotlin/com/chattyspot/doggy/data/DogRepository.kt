package com.chattyspot.doggy.data

import com.chattyspot.doggy.models.Dog
import org.springframework.data.repository.CrudRepository

interface DogRepository : CrudRepository<Dog, Long>