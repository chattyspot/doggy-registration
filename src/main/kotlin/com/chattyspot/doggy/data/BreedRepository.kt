package com.chattyspot.doggy.data

import com.chattyspot.doggy.models.Breed
import org.springframework.data.repository.CrudRepository

interface BreedRepository : CrudRepository<Breed, Long>