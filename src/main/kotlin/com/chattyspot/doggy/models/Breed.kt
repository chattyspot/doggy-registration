package com.chattyspot.doggy.models

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotEmpty

@Entity(name = "Breed")
data class Breed(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @field:NotEmpty(message = "Breed title must not be empty!")
        var title: String? = null
)