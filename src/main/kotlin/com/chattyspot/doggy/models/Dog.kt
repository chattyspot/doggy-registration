package com.chattyspot.doggy.models

import javax.persistence.*
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty

@Entity(name = "Dog")
data class Dog(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @field:NotEmpty(message = "The dog's name must not be empty!")
        var name: String? = null,

        @field:Min(value = 1, message = "The dog's age must be older than 0!")
        var age: Int? = null,

        @field:ManyToOne(targetEntity = Breed::class)
        var breed: Breed? = null,

        @field:ManyToMany(targetEntity = ServiceType::class)
        var serviceType: List<ServiceType>? = ArrayList()
)